#include "worker/include/fd.hpp"

#include <array>

namespace abeille {
namespace worker {
namespace fd {

error BidirectionalPipes::CreatePipes() noexcept {
  if (pipe(req) == -1) {
    return error("failed to create req pipe");
  }

  if (pipe(resp) == -1) {
    return error("failed to create resp pipe");
  }

  return error();
}

void BidirectionalPipes::ClosePipes() noexcept {
  close(req[0]);
  close(req[1]);
  close(resp[0]);
  close(resp[1]);
}

int GetReadChannel(const int pipe[2]) noexcept {
  return pipe[0];
}

int GetWriteChannel(const int pipe[2]) noexcept {
  return pipe[1];
}

void CloseReadChannel(int pipe[2]) noexcept {
  close(pipe[0]);
}

void CloseWriteChannel(int pipe[2]) noexcept {
  close(pipe[1]);
}

error ReadFromChannel(int channel, std::string &message) {
  std::array<char, BUFFER_SIZE> buffer;
  while (true) {
    ssize_t read_size = read(channel, buffer.data(), BUFFER_SIZE);
    if (read_size == -1) {
      return error("failed to read from the pipe");
    }

    message.append(buffer.data(), BUFFER_SIZE);

    if (read_size < BUFFER_SIZE) {
      break;
    }
  }

  return error();
}

error WriteToChannel(int channel, const std::string &message) {
  ssize_t write_size = write(channel, message.c_str(), message.size());
  if (write_size == -1) {
    return error("failed to write to the pipe");
  }

  if (write_size < message.size()) {
    return error("partial write to the pipe");
  }

  return error();
}

}  // namespace fd
}  // namespace worker
}  // namespace abeille
