#ifndef ABEILLE_WORKER_LIBS_REGISTRY_HPP_
#define ABEILLE_WORKER_LIBS_REGISTRY_HPP_

#include <filesystem>
#include <string>

#include "common/errors/include/errors.hpp"
#include "common/utils/include/types.hpp"

namespace abeille {
namespace worker {

class Registry {
 public:
  error Init(const std::string &filepath) noexcept;

  void SetLibsDirectory(const std::string &dirpath) noexcept;

  error SaveLibrary(uint64_t user_id, const Bytes &lib);

  std::string GetFilepath(uint64_t user_id) const noexcept;

 private:
  std::filesystem::path libs_dir_path_;
};

}  // namespace worker
}  // namespace abeille

#endif  // ABEILLE_WORKER_LIBS_REGISTRY_HPP_
