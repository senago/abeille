#ifndef ABEILLE_WORKER_IPC_HPP_
#define ABEILLE_WORKER_IPC_HPP_

#include <memory>

#include "common/errors/include/errors.hpp"
#include "common/utils/include/types.hpp"
#include "worker/include/fd.hpp"
#include "worker/include/registry.hpp"
#include "worker/proto/ipc.pb.h"

using namespace abeille::worker::fd;

namespace abeille {
namespace worker {

enum class IPCCommand { CreateMayfly };

class IPC {
 public:
  explicit IPC(std::shared_ptr<Registry> registry) noexcept;
  ~IPC() noexcept;

  error Run();
  void Shutdown() noexcept;

  error ProcessTask(const IPCRequest &request, Bytes &response);

 private:
  error runManagerJob();

  error createMayfly();
  error runMayflyJob();
  error processTaskData(uint64_t user_id, const Bytes &task_data,
                        Bytes &task_result) const;

 private:
  BidirectionalPipes stream_;
  BidirectionalPipes mayfly_stream_;

  bool shutdown_ = false;

  std::shared_ptr<Registry> registry_ = nullptr;
};

}  // namespace worker
}  // namespace abeille

#endif  // ABEILLE_WORKER_IPC_HPP_
