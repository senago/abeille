#ifndef ABEILLE_WORKER_FD_HPP_
#define ABEILLE_WORKER_FD_HPP_

#include <unistd.h>

#include <string>

#include "common/errors/include/errors.hpp"
namespace abeille {
namespace worker {
namespace fd {

static constexpr const size_t BUFFER_SIZE = 4096;

// BidirectionalPipes wraps file descriptors created by the system call pipe
struct BidirectionalPipes {
  error CreatePipes() noexcept;
  void ClosePipes() noexcept;

  int req[2];
  int resp[2];
};

int GetReadChannel(const int pipe[2]) noexcept;
int GetWriteChannel(const int pipe[2]) noexcept;

void CloseReadChannel(int pipe[2]) noexcept;
void CloseWriteChannel(int pipe[2]) noexcept;

error ReadFromChannel(int channel, std::string &message);
error WriteToChannel(int channel, const std::string &message);

template <typename POD>
error ReadPODFromChannel(int channel, POD &pod) {
  size_t bytes = sizeof(pod);
  ssize_t bytes_read = read(channel, &pod, bytes);

  if (bytes_read == -1) {
    return error("failed to read from the pipe");
  } else if (bytes_read < bytes) {
    return error("partial read from the pipe");
  }

  return error();
}

template <typename POD>
error WritePODToChannel(int channel, const POD &pod) {
  size_t bytes = sizeof(POD);
  ssize_t bytes_written = write(channel, &pod, bytes);

  if (bytes_written == -1) {
    return error("failed to write to the pipe");
  } else if (bytes_written < bytes) {
    return error("partial write to the pipe");
  }

  return error();
}

}  // namespace fd
}  // namespace worker
}  // namespace abeille

#endif  // ABEILLE_WORKER_FD_HPP_
