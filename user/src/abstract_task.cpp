#include "worker/include/abstract_task.hpp"

#include "common/utils/include/logger.hpp"
#include "user/include/task.hpp"

namespace abeille {
namespace worker {

error ProcessAbstractTaskData(const Bytes &task_data, Bytes &task_result) {
  Task::Data td;
  bool ok = td.ParseFromString(task_data);
  if (!ok) {
    return error("failed to parse from the string");
  }

  Task::Result tr;
  error err = abeille::user::ProcessTaskData(td, tr);
  if (!err.ok()) {
    return err;
  }

  task_result = tr.SerializeAsString();

  return error();
}

}  // namespace worker
}  // namespace abeille
