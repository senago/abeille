#include "user/include/api.hpp"

#include <google/protobuf/util/json_util.h>

#include "user/proto/task.pb.h"

using google::protobuf::util::JsonStringToMessage;

namespace abeille {
namespace user {

APIImpl::APIImpl(std::shared_ptr<Client> client) noexcept : client_(client) {}

error APIImpl::UploadTaskData(const std::string &filename, const std::string &json) noexcept {
  Task::Data task_data;
  auto status = JsonStringToMessage(json, &task_data);
  if (!status.ok()) {
    return error(status.message().as_string());
  }

  error err = client_->UploadData(filename, task_data.SerializeAsString());
  if (!err.ok()) {
    return err;
  }

  return error();
}

}  // namespace user
}  // namespace abeille
