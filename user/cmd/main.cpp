#include <google/protobuf/util/json_util.h>

#include <csignal>
#include <memory>
#include <string>

#include "common/utils/include/cli.hpp"
#include "linenoise.hpp"
#include "user/include/api.hpp"
#include "user/include/commands.hpp"
#include "user/include/registry.hpp"
#include "user/include/user_client.hpp"

using abeille::user::APIImpl;
using abeille::user::Client;
using abeille::user::CLIImpl;
using abeille::user::Registry;

bool shutdown = false;
std::shared_ptr<Registry> registry = nullptr;
std::shared_ptr<Client> client = nullptr;
std::shared_ptr<APIImpl> api = nullptr;
std::shared_ptr<CLIImpl> cli = nullptr;

void onShutdown() {
  client->Shutdown();
}

void signalHandler(int signal) {
  shutdown = true;
}

void listenShutdown() {
  while (!shutdown) {
    std::this_thread::sleep_for(std::chrono::seconds(2));
  }
  onShutdown();
}

void runCLI() {
  std::string line;
  while (!shutdown && !linenoise::Readline("abeille> ", line)) {
    bool exit = cli->Process(line);
    if (exit) {
      break;
    }
    linenoise::AddHistory(line.c_str());
  }
  shutdown = true;
}

error setup(char *argv[]) noexcept {
  std::string user_config_path = argv[1];
  registry = std::make_shared<Registry>();
  error err = registry->Init(user_config_path);
  if (!err.ok()) {
    return err;
  }

  std::string client_config_path = argv[2];
  client = std::make_shared<Client>(registry);
  err = client->Init(client_config_path);
  if (!err.ok()) {
    return err;
  }

  api = std::make_shared<APIImpl>(client);
  cli = std::make_shared<CLIImpl>(api);

  return error();
}

void run() noexcept {
  std::thread([&] {
    client->Run();
    if (!shutdown) {
      runCLI();
    }
  }).detach();
}

int main(int argc, char *argv[]) {
  if (argc != 3) {
    std::cout << "usage: [user config] and [client config]" << std::endl;
    return -1;
  }

  error err = setup(argv);
  if (!err.ok()) {
    std::cout << err.what() << std::endl;
    return -1;
  }

  run();

  std::signal(SIGINT, signalHandler);
  std::thread(listenShutdown).join();

  return 0;
}
