#ifndef ABEILLE_USER_API_HPP_
#define ABEILLE_USER_API_HPP_

#include <memory>

#include "common/errors/include/errors.hpp"
#include "user/include/user_client.hpp"

namespace abeille {
namespace user {

class API {
 public:
  virtual error UploadTaskData(const std::string &filename, const std::string &json) noexcept = 0;
};

class APIImpl : public API {
 public:
  explicit APIImpl(std::shared_ptr<Client> client) noexcept;

  error UploadTaskData(const std::string &filename, const std::string &json) noexcept override;

 private:
  std::shared_ptr<Client> client_;
};

}  // namespace user
}  // namespace abeille

#endif  // ABEILLE_USER_API_HPP_
