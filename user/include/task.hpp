#ifndef ABEILLE_USER_TASK_HPP_
#define ABEILLE_USER_TASK_HPP_

#include "common/errors/include/errors.hpp"
#include "user/proto/task.pb.h"

namespace abeille {
namespace user {

error ProcessTaskData(const Task::Data &task_data, Task::Result &task_result);

}  // namespace user
}  // namespace abeille

#endif  // ABEILLE_USER_TASK_HPP_
