#include "raft/rpc/include/worker_service.hpp"

#include <iterator>
#include <thread>

#include "common/utils/include/convert.hpp"
#include "common/utils/include/logger.hpp"

namespace abeille {
namespace raft {

void WorkerServiceImpl::ConnectHandler(uint64_t client_id) {
  // LOG_INFO("worker connection from [%s]", uint2address(client_id).c_str());
  client_ids_.push_back(client_id);
}

void WorkerServiceImpl::CommandHandler(uint64_t client_id, ConnResp &resp) {
  resp.set_leader_id(core_.raft_->GetLeaderID());

  // check if we are the leader
  if (!core_.raft_->IsLeader()) {
    // LOG_INFO("redirecting [%s] to the leader...",
    //        uint2address(client_id).c_str());
    resp.set_command(WORKER_COMMAND_REDIRECT);
    return;
  }

  auto &cw = client_wrappers_[client_id];
  if (cw.commands.empty()) {
    resp.set_command(WORKER_COMMAND_NONE);
    return;
  }

  auto command = cw.commands.front();
  resp.set_command(command);
  cw.commands.pop();

  error err;
  switch (command) {
    case WORKER_COMMAND_ASSIGN:
      err = handleCommandAssign(cw, resp);
      break;
    case WORKER_COMMAND_PROCESS:
      err = handleCommandProcess(cw, resp);
      break;
    default:
      break;
  }

  if (!err.ok()) {
    LOG_ERROR(err);
  }
}

error WorkerServiceImpl::handleCommandAssign(ClientWrapper &cw,
                                             ConnResp &resp) {
  resp.set_allocated_task_id(new TaskID(cw.task_wrapper.task_id()));
  return error();
}

error WorkerServiceImpl::handleCommandProcess(ClientWrapper &cw,
                                              ConnResp &resp) {
  if (cw.task_wrapper.task_data().empty()) {
    return error("empty task data");
  } else if (cw.task_wrapper.lib().empty()) {
    return error("empty lib");
  } else if (!cw.task_wrapper.has_task_id()) {
    return error("empty task id");
  }

  resp.set_allocated_task_id(new TaskID(cw.task_wrapper.task_id()));
  resp.set_task_data(cw.task_wrapper.task_data());
  resp.set_lib(cw.task_wrapper.lib());

  return error();
}

void WorkerServiceImpl::StatusHandler(uint64_t client_id, const ConnReq &req) {
  auto &cw = client_wrappers_[client_id];

  // update the status of the worker
  cw.status = req.status();

  error err;
  switch (cw.status) {
    case WORKER_STATUS_COMPLETED:
      err = handleStatusCompleted(cw, req);
      break;
    default:
      break;
  }

  if (!err.ok()) {
    LOG_ERROR(err);
  }
}

error WorkerServiceImpl::handleStatusCompleted(ClientWrapper &cw,
                                               const ConnReq &req) {
  if (!req.task_state().has_task_id()) {
    return error("empty task id");
  } else if (req.task_state().task_result().empty()) {
    return error("empty task result");
  }

  LOG_INFO("worker has finished task#[%s]",
           req.task_state().task_id().filename().c_str());
  core_.task_mgr_->UploadTaskResult(req.task_state().task_id(),
                                    req.task_state().task_result());

  return error();
}

error WorkerServiceImpl::AssignTask(const TaskID &task_id,
                                    uint64_t &worker_id) {
  worker_id = getActiveWorker();
  auto &cw = client_wrappers_[worker_id];
  cw.task_wrapper.set_allocated_task_id(new TaskID(task_id));
  cw.commands.push(WORKER_COMMAND_ASSIGN);
  return error();
}

error WorkerServiceImpl::ProcessTask(const TaskWrapper &task_wrapper) {
  uint64_t worker_id = task_wrapper.worker_id();
  auto it = client_wrappers_.find(worker_id);
  if (it == client_wrappers_.end()) {
    return error("worker with given id was not found");
  }

  it->second.commands.push(WORKER_COMMAND_PROCESS);
  it->second.task_wrapper = task_wrapper;

  LOG_DEBUG("successfully sent [%s] task to [%s]",
            task_wrapper.task_id().filename().c_str(),
            uint2address(worker_id).c_str());

  return error();
}

void WorkerServiceImpl::DisconnectHandler(uint64_t client_id) {
  client_wrappers_[client_id].status = WORKER_STATUS_DOWN;
  // LOG_WARN("connection with worker [%s] was lost",
  //         uint2address(client_id).c_str());
}

uint64_t WorkerServiceImpl::getActiveWorker() noexcept {
  while (client_ids_.empty() && !core_.shutdown_) {
    LOG_WARN("no workers are connected, waiting...");
    std::this_thread::sleep_for(std::chrono::seconds(3));
  }

  if (core_.shutdown_) {
    return 0;
  }

  // round-robin load balancing
  curr_client_index_ = (curr_client_index_ + 1) % client_ids_.size();
  auto status = client_wrappers_[client_ids_[curr_client_index_]].status;

  while (status == WORKER_STATUS_DOWN && !core_.shutdown_) {
    if (curr_client_index_ + 1 == client_ids_.size()) {
      LOG_INFO("no active worker to assign a task, retrying...");
      std::this_thread::sleep_for(std::chrono::seconds(3));
    }
    curr_client_index_ = (curr_client_index_ + 1) % client_ids_.size();
    status = client_wrappers_[client_ids_[curr_client_index_]].status;
  }

  return client_ids_[curr_client_index_];
}

}  // namespace raft
}  // namespace abeille
