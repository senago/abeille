#include "raft/core/include/core.hpp"

#include <grpcpp/grpcpp.h>

#include <memory>
#include <vector>

#include "common/utils/include/logger.hpp"
#include "raft/core/include/config.hpp"
#include "raft/rpc/include/raft_service.hpp"
#include "raft/rpc/include/user_service.hpp"
#include "raft/rpc/include/worker_service.hpp"

#define RaftS RaftServiceImpl
#define WorkerS WorkerServiceImpl
#define UserS UserServiceImpl

using abeille::rpc::ServiceInfo;

namespace abeille {
namespace raft {

Core::Core() noexcept
    : server_id_(0),
      shutdown_(false),
      raft_(nullptr),
      task_mgr_(nullptr),
      raft_server_(nullptr),
      raft_service_(nullptr),
      user_service_(nullptr),
      worker_service_(nullptr) {}

// This method must be called before any other one
// Config has to be checked before core initializing
Core::Status Core::Init() noexcept {
  Config &config = Config::GetInstance();
  std::string config_name = config.GetConfigName();
  std::ifstream input(config_name, std::ios::in | std::ios::binary);
  if (!input.is_open()) {
    LOG_ERROR("Unable to open FILE '%s'", config_name.c_str());
    return error(error::Code::FAILURE);
  }

  config.Init(input);
  input.close();

  if (!config.IsOk()) {
    LOG_ERROR("Config wasn't initialized");
    return Status(Status::Code::FAILURE);
  }

  LOG_INFO("Core initializing for %s", uint2address(config.GetId()).c_str());

  // Node initializing
  server_id_ = config.GetId();
  task_mgr_ = std::make_unique<TaskManager>(*this);
  raft_ = std::make_unique<RaftConsensus>(*this);

  // Services init
  raft_service_ = std::make_unique<RaftS>(GetRaft());
  worker_service_ = std::make_unique<WorkerS>(*this);
  user_service_ = std::make_unique<UserS>(GetRaft(), GetTaskManager());

  // Server init
  std::vector<ServiceInfo> services = {
      {config.GetRaftAddress(), raft_service_.get()},
      {config.GetUserAddress(), user_service_.get()},
      {config.GetWorkerAddress(), worker_service_.get()}};

  raft_server_ = std::make_unique<abeille::rpc::Server>(services);
  return Status();
}

void Core::Run() noexcept {
  LOG_INFO("Launching raft module...");
  raft_->Run();

  Status server_status = raft_server_->Run();
  if (!server_status.ok()) {
    LOG_ERROR("Server wasn't started. Shutting down raft node ...");
    Shutdown();
  }
}

void Core::Shutdown() noexcept {
  if (!shutdown_) {
    LOG_INFO("Shutting down all instances...");
    shutdown_ = true;

    if (raft_server_) {
      raft_server_->Shutdown();
    }

    if (raft_) {
      raft_->Shutdown();
    }
  }
}

Core::~Core() {
  Shutdown();
}

}  // namespace raft
}  // namespace abeille
