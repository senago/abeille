#ifndef ABEILLE_RAFT_CONFIG_HPP_
#define ABEILLE_RAFT_CONFIG_HPP_

#include <fstream>
#include <memory>
#include <mutex>
#include <string>
#include <vector>

#include "common/config/proto/config.pb.h"
#include "common/rpc/proto/abeille.grpc.pb.h"
#include "common/utils/include/logger.hpp"
#include "common/utils/include/types.hpp"

namespace abeille {
namespace raft {

// GetInstance is thread safe, setters are not
// Config file will overwrite all settings (if the will be encountered)
class Config {
  static std::shared_ptr<Config> instance_;
  static std::once_flag flag_;

  Config() noexcept = default;
  Config(const Config &) = delete;
  Config &operator=(const Config &) = delete;

 public:
  using AddressContainer = std::vector<std::string>;

  static Config &GetInstance() {
    std::call_once(flag_, []() { Config::instance_.reset(new Config()); });
    return *Config::instance_;
  };

  bool IsOk() const noexcept;
  void Init(std::ifstream &file);

  const uint64_t &GetId() const noexcept;
  const std::string &GetWorkerAddress() const noexcept;
  const std::string &GetUserAddress() const noexcept;
  const std::string &GetRaftAddress() const noexcept;
  const AddressContainer &GetPeers() const noexcept;
  const Index &GetSnapshotAfter() const noexcept;
  const std::string &GetConfigName() const noexcept;
  const std::string &GetSnapshotTo() const noexcept;

  // for future cli feature: getting config file of the node remotely
  RaftConfig *GetProtoConfig() noexcept {
    return &proto_config_;
  }

  void SetConfigName(std::string &str) noexcept;
  void SetSnapshotAfter(uint64_t after) noexcept;
  void SetSnapshotTo(std::string &str) noexcept;

  ~Config() = default;

 private:
  void protoInit(const RaftConfig &proto) noexcept;

  // addresses
  AddressContainer peers_;
  std::string user_address_;
  std::string raft_address_;
  std::string worker_address_;

  // snapshoting configuration
  std::string snapshot_to;
  Index snapshot_after_ = 20;

  // other needed info
  RaftConfig proto_config_;
  ServerId id_ = 0;
  std::string config_file_name_;
};

}  // namespace raft
}  // namespace abeille

#endif  // ABEILLE_RAFT_CONFIG_HPP_
