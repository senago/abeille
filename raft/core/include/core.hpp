#ifndef ABEILLE_RAFT_CORE_H_
#define ABEILLE_RAFT_CORE_H_

#include <grpcpp/grpcpp.h>

#include <atomic>
#include <condition_variable>
#include <memory>

#include "common/errors/include/errors.hpp"
#include "common/rpc/include/server.hpp"
#include "raft/consensus/raft/include/raft_consensus.hpp"
#include "raft/consensus/raft_pool/include/raft_pool.hpp"
#include "raft/core/include/config.hpp"
#include "raft/rpc/include/raft_service.hpp"
#include "raft/rpc/include/user_service.hpp"
#include "raft/rpc/include/worker_service.hpp"
#include "raft/task_manager/include/task_manager.hpp"

using abeille::rpc::Server;

namespace abeille {
namespace raft {

// forward declaration
class TaskManager;
class RaftPool;
class RaftServiceImpl;
class UserServiceImpl;
class WorkerServiceImpl;

// Holds the Abeille raft server's daemon's top-level objects
// The purpose of main() is to create, init and run a Core object
class Core {
 public:
  typedef RaftConsensus &RaftRef;
  typedef TaskManager &TaskMgrRef;
  typedef std::unique_ptr<RaftConsensus> RaftPtr;
  typedef std::unique_ptr<TaskManager> TaskMgrPtr;
  typedef std::unique_ptr<grpc::Service> ServicePtr;
  typedef std::unique_ptr<Server> ServerPtr;
  typedef error Status;

  Core() noexcept;
  Status Init() noexcept;
  ~Core();

  // Launches/ shutdowsn all threads from every top-level object
  void Run() noexcept;
  void Shutdown() noexcept;

  ServerId GetId() const noexcept {
    return server_id_;
  }
  TaskMgrRef GetTaskManager() noexcept {
    return *task_mgr_.get();
  }
  RaftRef GetRaft() noexcept {
    return *raft_.get();
  }

  // Non-copyable object
  Core(const Core &) = delete;
  Core(Core &&other) = delete;
  Core &operator=(const Core &) = delete;

 private:
  uint64_t server_id_;
  bool shutdown_;

  RaftPtr raft_;
  TaskMgrPtr task_mgr_;

  // Process inbound RPCs
  ServerPtr raft_server_;
  ServicePtr raft_service_;
  ServicePtr user_service_;
  ServicePtr worker_service_;

  friend class Worker;
  friend class TaskManager;
  friend class WorkerServiceImpl;
};

}  // namespace raft
}  // namespace abeille

#endif  // ABEILLE_RAFT_CORE_H_
