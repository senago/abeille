add_subdirectory(rpc)
add_subdirectory(core)
add_subdirectory(consensus)
add_subdirectory(task_manager)

add_executable(abeille-raft cmd/src/main.cpp)
target_include_directories(abeille-raft PUBLIC ${PROJECT_SOURCE_DIR})
target_link_libraries(abeille-raft core)
