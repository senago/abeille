#include "raft/consensus/state_machine/include/snapshot_ipc.hpp"

#include <cstring>
#include <system_error>

#include "common/utils/include/logger.hpp"

namespace abeille {
namespace raft {

SnapshotIPC::SnapshotIPC() {
  if ((msgqid = msgget(IPC_PRIVATE, IPC_CREAT | 0660)) == -1) {
    throw std::system_error(errno, std::generic_category(),
                            std::strerror(errno));
  }
}

SnapshotIPC::Status SnapshotIPC::SendIndex(Index idx) const noexcept {
  auto failure = Status(Status::Code::FAILURE);
  if (msgqid == -1) {
    LOG_ERROR("No message queue was created");
    return failure;
  }

  Message msg;
  msg.type = 1;
  strcpy(msg.text, std::to_string(idx).c_str());

  if (msgsnd(msgqid, (void *)&msg, sizeof(msg.text), IPC_NOWAIT) == -1) {
    LOG_ERROR("Error while sending IPC message from fork");
    return failure;
  }

  return Status();
}

Index SnapshotIPC::GetIndex() const noexcept {
  Index index = 0;
  if (msgqid == -1) {
    LOG_ERROR("No message queue was created");
    return index;
  }

  // if not in queue the calling thread will return immediately
  Message msg;
  if (msgrcv(msgqid, (void *)&msg, MAX_SEND_SIZE, 1, IPC_NOWAIT) == -1) {
    return index;
  }

  index = std::stoll(msg.text);
  return index;
}

}  // namespace raft
}  // namespace abeille
