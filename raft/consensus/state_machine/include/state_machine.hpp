#ifndef ABEILLE_RAFT_STATE_MACHINE_H_
#define ABEILLE_RAFT_STATE_MACHINE_H_

#include <functional>
#include <unordered_map>

#include "common/errors/include/errors.hpp"
#include "raft/consensus/log/include/log.hpp"
#include "raft/consensus/raft/include/raft_consensus.hpp"
#include "raft/consensus/state_machine/include/snapshot_ipc.hpp"
#include "raft/task_manager/include/task_manager.hpp"

namespace abeille {
namespace raft {

class TaskManager;

/*
struct TaskIDWrapper {
TaskIDWrapper(const std::string& filename, uint64_t client_id) noexcept
    : filename_(filename), client_id_(client_id) {}

bool operator==(const TaskIDWrapper& other) const noexcept {
  return filename_ == other.filename_ && client_id_ == other.client_id_;
}

struct Hash {
  size_t operator()(const TaskIDWrapper& tw) const noexcept {
    return std::hash<std::string>{}(tw.filename_ +
                                    std::to_string(tw.client_id_));
  }
};

std::string filename_;
uint64_t client_id_ = 0;
};
*/

struct TaskIDWrapper {
  TaskIDWrapper(const TaskID &id) noexcept : id_(id) {}

  bool operator==(const TaskIDWrapper &other) const noexcept {
    return id_.filename() == other.id_.filename() &&
           id_.client_id() == other.id_.client_id();
  }

  struct Hash {
    size_t operator()(const TaskIDWrapper &tw) const noexcept {
      return std::hash<std::string>{}(tw.id_.filename() +
                                      std::to_string(tw.id_.client_id()));
    }
  };

  TaskID id_;
};

class StateMachine {
 public:
  typedef Log &LogRef;
  typedef error Status;
  typedef TaskManager &TaskMgrRef;
  typedef std::chrono::steady_clock Clock;
  typedef std::unique_ptr<Snapshot> SnapshotPtr;
  typedef std::unordered_map<TaskIDWrapper, TaskWrapper, TaskIDWrapper::Hash>
      HashTable;

  explicit StateMachine(LogRef log, TaskMgrRef mgr) : log_(log), mgr_(mgr) {}

  // const getters are thread-safe
  Term GetCommitTerm() const noexcept {
    return commit_term_;
  }
  Index GetCommitIndex() const noexcept {
    return commit_index_;
  }
  Term GetSnapshotTerm() const noexcept {
    return snapshotted_term_;
  }
  Index GetSnapshotIndex() const noexcept {
    return snapshotted_index_;
  }

  void Commit(Index index, bool is_leader) noexcept;
  void Reset(const Snapshot &snapshot) noexcept;
  // this one is pretty long synchronous operation
  SnapshotPtr GetSnapshot() const noexcept;
  Status SaveSnapshot(const Snapshot &snapshot) noexcept;
  bool IsSnapshotting() const noexcept {
    return snapshotting;
  }

  void Run() noexcept;
  void Shutdown() noexcept;

 private:
  Status applyCommand(Log::EntryConstReference entry, bool is_leader) noexcept;
  void snapshotThreadMain() noexcept;
  void updateLog(Index idx, Term term) noexcept;
  void snapshot() noexcept;
  void forkedSnapshot(HashTable assigned, HashTable completed,
                      Index commit_index, Term commit_term) noexcept;

  // Reaching consensus on this data:
  HashTable assigned_;
  HashTable completed_;

  // Index of the last commited entry/term
  Index commit_index_ = 0;
  Term commit_term_ = 0;

  // For snapshotting purposes
  std::mutex mutex_;
  bool exiting_ = false;
  bool snapshotting = false;
  Index snapshotted_index_ = 0;
  Term snapshotted_term_ = 0;
  std::thread snapshot_thread_;
  // std::thread fork_snapshot_thread_;
  Config &config_ = Config::GetInstance();
  std::condition_variable state_changed_;
  // std::unique_ptr<SnapshotIPC> ipc_ = nullptr;

  LogRef log_;
  TaskMgrRef mgr_;
};

}  // namespace raft
}  // namespace abeille

#endif  // ABEILLE_RAFT_STATE_MACHINE_H_
