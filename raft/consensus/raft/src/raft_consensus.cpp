#include "raft/consensus/raft/include/raft_consensus.hpp"

#include <google/protobuf/util/json_util.h>
#include <grpcpp/grpcpp.h>

#include <chrono>
#include <cstdlib>

#include "common/rpc/proto/abeille.grpc.pb.h"
#include "common/utils/include/logger.hpp"
#include "common/utils/include/types.hpp"
#include "raft/core/include/core.hpp"
using namespace google::protobuf::util;
using namespace std::chrono_literals;

namespace abeille {
namespace raft {

RaftConsensus::RaftConsensus(RaftConsensus::CoreRef core) noexcept
    : id_(core.GetId()),
      core_(core),
      log_(new Log()),
      state_machine_(new StateMachine(*log_, core_.GetTaskManager())),
      raft_pool_(new RaftPool(*this)),
      num_peers_threads_(0) {}

RaftConsensus::~RaftConsensus() {
  Shutdown();
}

void RaftConsensus::Run() noexcept {
  raft_pool_->Run();
  state_machine_->Run();
  timer_thread_ = std::thread(&RaftConsensus::timerThreadMain, this);
  LOG_INFO("RaftConsensus for server %s was started",
           uint2address(id_).c_str());
}

void RaftConsensus::AppendEntry(const Entry &entry) noexcept {
  log_->Append(entry);
  raft_state_changed_.notify_all();
}

void RaftConsensus::stepDown(uint64_t new_term) {
  /*
  if (state_ == State::CANDIDATE) {
    LOG_INFO("Stepping down from CANDIDATE to FOLLOWER");
  }
  */

  if (current_term_ < new_term) {
    LOG_INFO("Stepping down to %lu", new_term);
    current_term_ = new_term;
    leader_id_ = 0;
    voted_for_ = 0;
    state_ = State::FOLLOWER;

  } else if (state_ != State::FOLLOWER) {
    state_ = State::FOLLOWER;
  }

  // was leader
  if (start_new_election_at_ == TimePoint::max()) {
    resetElectionTimer();
  }

  if (hold_election_for == TimePoint::max()) {
    hold_election_for = TimePoint::min();
  }

  raft_state_changed_.notify_all();
}

// If election timeout elapses without recieving AppendEntries
// initiates startNewElection()
void RaftConsensus::timerThreadMain() {
  std::unique_lock<std::mutex> lock_guard(mutex_);
  resetElectionTimer();
  while (!shutdown_) {
    if (Clock::now() >= start_new_election_at_) {
      startNewElection();
    }

    raft_state_changed_.wait_until(lock_guard, start_new_election_at_);
    // [this]() { return shutdown_; });
  }
}

void RaftConsensus::Shutdown() noexcept {
  if (!shutdown_) {
    LOG_INFO("Shutting down raft...");
    shutdown_ = true;

    raft_pool_->Shutdown();
    LOG_INFO("Raft pool was shutted down");

    if (state_machine_->IsSnapshotting()) {
      LOG_WARN(
          "State machine is snapshotting."
          "Cannot send snapshot right now. Waiting...");
      while (state_machine_->IsSnapshotting()) {
        std::this_thread::sleep_for(RPC_DEADLINE_);
      }
    }

    state_machine_->Shutdown();
    LOG_INFO("State machine was shutted down");

    raft_state_changed_.notify_all();
    if (timer_thread_.joinable()) {
      timer_thread_.join();
    }
    LOG_INFO("Timer thread was shutted down");

    LOG_INFO("Waiting for threads to finish...");
    std::unique_lock<std::mutex> lock_guard(mutex_);
    while (num_peers_threads_ > 0) {
      raft_state_changed_.wait(lock_guard);
    }

    LOG_INFO("All peer's threads have been shutted down");
  }
}

void RaftConsensus::resetElectionTimer() {
  // election timeout is 150 - 300 ms
  uint64_t rand_duration =
      rand() % (ELECTION_TIMEOUT_.count()) + ELECTION_TIMEOUT_.count();
  std::chrono::milliseconds duration(rand_duration);
  start_new_election_at_ = Clock::now() + duration;
  raft_state_changed_.notify_all();
}

void RaftConsensus::becomeLeader() {
  LOG_INFO("I'm the leader now (term %lu)", current_term_);
  state_ = State::LEADER;
  leader_id_ = id_;
  start_new_election_at_ = TimePoint::max();
  hold_election_for = TimePoint::max();
  raft_pool_->UpdatePeers();
  raft_state_changed_.notify_all();
}

void RaftConsensus::startNewElection() {
  if (leader_id_ > 0) {
    LOG_INFO(
        "Starting election in term %lu (haven't heard from leader %lu lately",
        current_term_ + 1, uint2address(leader_id_).c_str());
  } else if (state_ == State::CANDIDATE) {
    LOG_INFO("Starting election in term %lu (previous term %lu timed out)",
             current_term_ + 1, current_term_);
  } else {
    LOG_INFO("Starting election in term %lu", current_term_ + 1);
  }

  ++current_term_;
  state_ = State::CANDIDATE;
  voted_for_ = id_;
  leader_id_ = 0;
  resetElectionTimer();
  raft_pool_->BeginRequestVote();
  raft_state_changed_.notify_all();

  // if we are the only server
  if (raft_pool_->MajorityVotes()) becomeLeader();
}

void RaftConsensus::commitEntry(Index index) {
  Index last_log_index = log_->LastIndex();
  Index max_idx = last_log_index >= index ? index : last_log_index;
  state_machine_->Commit(max_idx, IsLeader());
}

void RaftConsensus::advanceCommitIndex() {
  if (state_ != State::LEADER) {
    LOG_WARN("Advancing commit initiated NOT by the leader");
  }

  Index commit_idx = state_machine_->GetCommitIndex();
  // LOG_DEBUG("Commit idx from state machine: %lu", commit_idx);
  Index new_commit_idx = raft_pool_->PoolCommitIndex(commit_idx);
  // LOG_DEBUG("Commit idx from RaftPool: %lu", new_commit_idx);

  if (commit_idx >= new_commit_idx) {
    return;
  }

  if ((*log_)[new_commit_idx].term() != current_term_) {
    return;
  }

  commitEntry(new_commit_idx);
  raft_state_changed_.notify_all();
}

void RaftConsensus::appendEntry(std::unique_lock<std::mutex> &lck, Peer &peer) {
  assert(state_ == State::LEADER);
  // LOG_DEBUG("Appending entry to %lu peer...",
  // uint2address(peer.id_).c_str());

  if (log_->StartIndex() > peer.next_index_) {
    installSnapshot(lck, peer);
    return;
  }

  Term prev_log_term;
  Index prev_log_idx = peer.next_index_ - 1;
  if (prev_log_idx == 0) {
    prev_log_term = 0;
  } else if (prev_log_idx >= log_->StartIndex()) {
    if (log_->HasEntryWith(prev_log_idx)) {
      prev_log_term = (*log_)[prev_log_idx].term();
    } else {
      installSnapshot(lck, peer);
      return;
    }

  } else if (prev_log_idx == state_machine_->GetSnapshotIndex()) {
    prev_log_term = state_machine_->GetSnapshotTerm();
  } else {
    installSnapshot(lck, peer);
    return;
  }

  AppendEntryRequest request;
  request.set_term(current_term_);
  request.set_leader_id(id_);
  request.set_prev_log_index(prev_log_idx);
  request.set_prev_log_term(prev_log_term);
  request.set_leader_commit(state_machine_->GetCommitIndex());

  Entry to_peer;
  uint8_t entries_num = 0;
  if (log_->LastIndex() >= peer.next_index_ &&
      !(state_machine_->IsSnapshotting())) {
    to_peer = (*log_)[peer.next_index_];
    request.set_allocated_entry(&to_peer);
    ++entries_num;
    assert(request.has_entry());
  }

  AppendEntryResponse response;
  grpc::ClientContext context;
  context.set_deadline(std::chrono::system_clock::now() + RPC_DEADLINE_);
  TimePoint now = Clock::now();

  lck.unlock();
  grpc::Status status = peer.stub_->AppendEntry(&context, request, &response);
  lck.lock();

  request.release_entry();

  // if cannot communicate for logging purposes
  if (!status.ok() && peer.failed_rpc_num_ == 0) {
    LOG_WARN("appendEntry failed: Unable to communicate with %s",
             uint2address(peer.id_).c_str());
  }

  if (!status.ok()) {
    ++peer.failed_rpc_num_;
    peer.prorogue_for_ = now + FAILURE_PROROGUE_;
    return;
  }

  // Success rpc
  if (peer.failed_rpc_num_ != 0) {
    // LOG_DEBUG("RPC to peer %s succeeded after %lu failures",
    //           uint2address(peer.id_).c_str(), peer.failed_rpc_num_);
    peer.failed_rpc_num_ = 0;
  }

  if (current_term_ != request.term() || peer.exiting_) {
    return;
  }

  if (response.term() > current_term_) {
    LOG_INFO("Recieved AppendEntry response with term %lu. Updating...",
             response.term());
    stepDown(response.term());

  } else {
    peer.next_heartbeat_time_ = now + HEARTBEAT_PERIOD_;
    raft_state_changed_.notify_all();

    if (response.success()) {
      peer.match_index_ = peer.next_index_ - 1 + entries_num;
      peer.next_index_ = peer.match_index_ + 1;
      advanceCommitIndex();
    } else if (peer.next_index_ > 1) {
      --peer.next_index_;
    }
  }
}

void RaftConsensus::requestVote(std::unique_lock<std::mutex> &lck, Peer &peer) {
  RequestVoteRequest request;
  request.set_term(current_term_);
  request.set_candidate_id(id_);
  request.set_last_log_entry(log_->LastIndex());
  request.set_last_log_term(getLastLogTerm());

  RequestVoteResponse response;
  grpc::ClientContext context;
  context.set_deadline(std::chrono::system_clock::now() + RPC_DEADLINE_);
  TimePoint now = Clock::now();

  lck.unlock();
  auto status = peer.stub_->RequestVote(&context, request, &response);
  lck.lock();

  // if cannot communicate
  // for logging purposes
  if (!status.ok() && peer.failed_rpc_num_ == 0) {
    LOG_WARN("requestVote failed: Unable to communicate with %s",
             uint2address(peer.id_).c_str());
  }

  if (!status.ok()) {
    ++peer.failed_rpc_num_;
    peer.prorogue_for_ = now + FAILURE_PROROGUE_;
    return;
  }

  // Success rpc
  if (peer.failed_rpc_num_ != 0) {
    // LOG_DEBUG("RPC to peer %s succeeded after %lu failures",
    //           uint2address(peer.id_).c_str(), peer.failed_rpc_num_);
    peer.failed_rpc_num_ = 0;
  }

  if (current_term_ != request.term() || state_ != State::CANDIDATE ||
      peer.exiting_)
    return;

  if (response.term() > current_term_)
    stepDown(response.term());
  else {
    peer.vote_request_done_ = true;
    raft_state_changed_.notify_all();

    if (response.vote_granted()) {
      peer.have_vote_ = true;
      raft_state_changed_.notify_all();
      if (raft_pool_->MajorityVotes()) {
        becomeLeader();
      }

    } else {
      // LOG_INFO("RequestVote was declined by %s",
      //         uint2address(peer.id_).c_str());
      peer.prorogue_for_ = now + ELECTION_TIMEOUT_;
    }
  }
}

void RaftConsensus::installSnapshot(std::unique_lock<std::mutex> &lck,
                                    Peer &peer) {
  if (state_machine_->IsSnapshotting()) {
    lck.unlock();

    LOG_WARN(
        "State machine is snapshotting. "
        "Cannot send snapshot right now. Waiting...");
    while (state_machine_->IsSnapshotting()) {
      std::this_thread::sleep_for(RPC_DEADLINE_);
    }

    lck.lock();
  }

  Index snapshot_idx = state_machine_->GetSnapshotIndex();
  Term snapshot_term = state_machine_->GetSnapshotTerm();
  auto snapshot(state_machine_->GetSnapshot());
  if (snapshot == nullptr) {
    LOG_ERROR("PANIC. No snapshot file");
    return;
  }

  // request filling
  InstallSnapshotRequest request;
  request.set_term(current_term_);
  request.set_leader_id(leader_id_);
  request.set_last_index(snapshot_idx);
  request.set_last_term(snapshot_term);
  request.set_allocated_snapshot(snapshot.get());

  // execute rpc
  grpc::ClientContext context;
  context.set_deadline(std::chrono::system_clock::now() + RPC_DEADLINE_);
  InstallSnapshotResponse response;
  TimePoint now = Clock::now();
  // LOG_DEBUG("Sending snapshot to %lu peer", peer.id_);
  lck.unlock();
  grpc::Status status =
      peer.stub_->InstallSnapshot(&context, request, &response);
  lck.lock();
  request.release_snapshot();

  // if cannot communicate
  // for logging purposes
  if (!status.ok() && peer.failed_rpc_num_ == 0) {
    LOG_WARN("installSnapshot failed: Unable to communicate with %s",
             uint2address(peer.id_).c_str());
  }

  if (!status.ok()) {
    ++peer.failed_rpc_num_;
    peer.prorogue_for_ = now + FAILURE_PROROGUE_;
    return;
  }

  // Success rpc
  if (peer.failed_rpc_num_ != 0) {
    // LOG_DEBUG("RPC to peer %s succeeded after %lu failures",
    //           uint2address(peer.id_).c_str(), peer.failed_rpc_num_);
    peer.failed_rpc_num_ = 0;
  }

  // Processing response
  if (current_term_ != request.term() || peer.exiting_) {
    return;
  }

  if (response.term() > current_term_) {
    LOG_INFO("Recieved InstallSnapshot response with term %lu. Updating...",
             response.term());
    stepDown(response.term());

  } else if (response.success()) {
    peer.next_heartbeat_time_ = now + HEARTBEAT_PERIOD_;
    raft_state_changed_.notify_all();
    peer.match_index_ = snapshot_idx;
    peer.next_index_ = snapshot_idx + 1;
    advanceCommitIndex();
  } else {
    LOG_WARN("InstallSnapshot to %lu was unsuccessfull", peer.id_);
  }
}

void RaftConsensus::peerThreadMain(std::shared_ptr<Peer> peer) {
  LOG_INFO("Peer thread for [%s] was started", uint2address(peer->id_).c_str());
  ++num_peers_threads_;

  std::unique_lock<std::mutex> lock_guard(mutex_);

  // issue RPC or sleeps on the cv
  while (!peer->exiting_) {
    TimePoint now = Clock::now();
    TimePoint wait_until = TimePoint::min();

    if (peer->prorogue_for_ > now) {
      wait_until = peer->prorogue_for_;

    } else {
      switch (state_) {
        case State::FOLLOWER:
          wait_until = TimePoint::max();
          break;

        case State::CANDIDATE:
          if (!peer->vote_request_done_) {
            requestVote(lock_guard, *peer);
          } else {
            wait_until = TimePoint::max();
          }

          break;

        case State::LEADER:
          if (peer->next_heartbeat_time_ <= now) {
            appendEntry(lock_guard, *peer);
          } else {
            wait_until = peer->next_heartbeat_time_;
          }

          break;
      }
    }

    raft_state_changed_.wait_until(lock_guard, wait_until);
  }

  --num_peers_threads_;
  raft_state_changed_.notify_all();
}

void RaftConsensus::HandleAppendEntry(const AppendEntryRequest *msg,
                                      AppendEntryResponse *resp) {
  // LOG_DEBUG("AppendEntry request was recieved from %lu", msg->leader_id());
  std::lock_guard<std::mutex> lockGuard(mutex_);

  /*
  std::string msg_str;
  MessageToJsonString(*msg, &msg_str);
  std::cout << msg_str << std::endl;
  */

  // Set response to rejection at first. Will be changed
  resp->set_term(current_term_);
  resp->set_success(false);

  if (msg->term() < current_term_) {
    LOG_INFO("AppendEntry caller is stale");
    return;
  }

  if (msg->term() > current_term_) {
    LOG_INFO("Recieved AppendEntry with %lu term\n",
             "My term is %lu. Updating term...", msg->term(), current_term_);
    // Our term will be updated in stepDown
    resp->set_term(msg->term());
  }

  // To follower state, update term, reset timer
  stepDown(msg->term());
  resetElectionTimer();
  hold_election_for = Clock::now() + ELECTION_TIMEOUT_;

  // Record LeaderId
  if (leader_id_ == 0) {
    leader_id_ = msg->leader_id();
    LOG_INFO("New leader is [%s]", uint2address(leader_id_).c_str());
  }

  if (msg->prev_log_index() > log_->LastIndex()) {
    LOG_INFO("Rejecting AppendEntry... Recieved %lu index. Mine is %lu",
             msg->prev_log_index(), log_->LastIndex());
    return;
  }

  if (msg->prev_log_index() >= log_->StartIndex() &&
      msg->prev_log_term() != (*log_)[log_->LastIndex()].term()) {
    LOG_INFO("Rejecting AppendEntry... Terms don't agree");
    return;
  }

  // Applying AppendEntry
  resp->set_success(true);
  if (msg->has_entry()) {
    LOG_DEBUG("Logging task ...");
    size_t num = log_->DropAfter(msg->prev_log_index());
    if (num != 0) {
      LOG_INFO("Dropping %lu entries from log", num);
    }

    log_->Append(msg->entry());
  }

  if (state_machine_->GetCommitIndex() < msg->leader_commit()) {
    LOG_INFO("Commiting new entries");
    commitEntry(msg->leader_commit());
  }
}

Term RaftConsensus::getLastLogTerm() noexcept {
  Term last_idx = log_->LastIndex();
  if (last_idx >= log_->StartIndex()) {
    return (*log_)[last_idx].term();
  }

  return state_machine_->GetSnapshotIndex();
}

void RaftConsensus::HandleRequestVote(const RequestVoteRequest *msg,
                                      RequestVoteResponse *resp) {
  std::lock_guard<std::mutex> lockGuard(mutex_);
  // LOG_INFO("RequestVote from candidate %lu was recieved",
  //         uint2address(msg->candidate_id()).c_str());
  Term last_term = getLastLogTerm();

  bool is_ok = (msg->last_log_term() > last_term ||
                (msg->last_log_term() == last_term &&
                 msg->last_log_entry() >= log_->LastIndex()));

  if (hold_election_for > Clock::now()) {
    LOG_INFO("Rejecting RequestVote from %lu ... Heard from a leader recently",
             uint2address(msg->candidate_id()).c_str());
    resp->set_term(current_term_);
    resp->set_vote_granted(false);
    return;
  }

  if (msg->term() > current_term_) {
    LOG_INFO("Recieved RequestVote. Changing term of the current server...");
    stepDown(msg->term());
  }

  if (msg->term() == current_term_ && is_ok && !voted_for_) {
    LOG_INFO("Voting for [%s]", uint2address(msg->candidate_id()).c_str());
    stepDown(current_term_);
    resetElectionTimer();
    voted_for_ = msg->candidate_id();
  }

  resp->set_term(current_term_);
  resp->set_vote_granted(voted_for_ == msg->candidate_id());
}

void RaftConsensus::HandleInstallSnapshot(const InstallSnapshotRequest *msg,
                                          InstallSnapshotResponse *resp) {
  LOG_INFO("Install snapshot from [%s] was recieved",
           uint2address(msg->leader_id()).c_str());
  std::lock_guard<std::mutex> lockGuard(mutex_);

  resp->set_term(current_term_);
  resp->set_success(false);
  if (msg->term() < current_term_) {
    LOG_INFO("InstallSnapshot caller is stale");
    return;
  }

  if (msg->term() > current_term_) {
    LOG_INFO("Recieved InstallSnapshot with %lu term\n",
             "Our term is %lu. Updating term...", msg->term(), current_term_);
    // Our term will be updated in stepDown
    resp->set_term(msg->term());
  }

  // To follower state, update term, reset timer
  stepDown(msg->term());
  resetElectionTimer();
  hold_election_for = Clock::now() + ELECTION_TIMEOUT_;

  // Record LeaderId
  if (leader_id_ == 0) {
    leader_id_ = msg->leader_id();
    LOG_INFO("All heil new leader [%s]", uint2address(leader_id_).c_str());
  }

  if (state_machine_->IsSnapshotting()) {
    LOG_WARN("State machine is snapshotting. Cannot accept snapshot");
    return;
  }

  auto status = state_machine_->SaveSnapshot(msg->snapshot());
  if (!status.ok()) {
    LOG_ERROR("Unsuccessful snapshot saving");
    return;
  } else {
    LOG_INFO("Snapshot was saved successfully");
  }

  if (log_->HasEntryWith(msg->snapshot().last_index(),
                         msg->snapshot().last_term())) {
    size_t count = log_->DropBefore(msg->snapshot().last_index() + 1);
    if (count) {
      LOG_INFO("Dropped %zu entries from log", count);
    }

    return;
  }

  size_t count = log_->Discard();
  if (count) {
    LOG_INFO("Discarded %zu entries from log", count);
  }

  state_machine_->Reset(msg->snapshot());
  log_->SetStartIndex(msg->snapshot().last_index() + 1);
  LOG_INFO("Last index of log now is %lu", log_->LastIndex());
  resp->set_success(true);
}

}  // namespace raft
}  // namespace abeille
