#ifndef ABEILLE_RAFT_RAFT_POOL_H_
#define ABEILLE_RAFT_RAFT_POOL_H_
#include <cstdint>
#include <functional>
#include <memory>
#include <unordered_map>

#include "common/errors/include/errors.hpp"
#include "common/rpc/proto/abeille.pb.h"
#include "common/utils/include/types.hpp"
#include "raft/consensus/raft/include/raft_consensus.hpp"
#include "raft/consensus/raft_pool/include/peer.hpp"
#include "raft/core/include/core.hpp"

namespace abeille {
namespace raft {

// forward declaration
class RaftConsensus;
class Peer;

class RaftPool {
 public:
  typedef std::shared_ptr<Peer> PeerRef;
  typedef RaftConsensus &RaftRef;

  // Init all peers with raft_consensus
  explicit RaftPool(RaftRef raft) noexcept;
  ~RaftPool() = default;

  // Launches peer_thread_main from raft_consensus for all peers
  void Run() noexcept;
  void Shutdown() noexcept;

  // Updating next idx & commit idx
  void UpdatePeers() noexcept;
  Index PoolCommitIndex(Index &prev_commit_idx) noexcept;
  void BeginRequestVote() noexcept;
  bool MajorityVotes() const noexcept;

 private:
  std::vector<PeerRef> peers_;
  RaftRef raft_;
};

}  // namespace raft
}  // namespace abeille

#endif  // ABEILLE_RAFT_RAFT_POOL_H_
