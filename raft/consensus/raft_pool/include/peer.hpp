#ifndef ABEILLE_RAFT_PEER_H_
#define ABEILLE_RAFT_PEER_H_

#include <grpcpp/channel.h>

#include <chrono>
#include <memory>
#include <queue>
#include <thread>

#include "common/rpc/proto/abeille.grpc.pb.h"
#include "common/utils/include/types.hpp"
#include "raft/consensus/raft/include/raft_consensus.hpp"
#include "raft/rpc/include/raft_service.hpp"

namespace abeille {
namespace raft {

// forward declaration
class RaftConsensus;

// Tracks various bits of state for each server, wich is used
// when we are the candidate or the leader
class Peer {
 public:
  typedef std::chrono::steady_clock::time_point TimePoint;
  typedef RaftConsensus &RaftRef;

  // grpc stub is initialized with channel
  explicit Peer(std::shared_ptr<grpc::Channel> channel, RaftRef raft,
                uint64_t id) noexcept;
  ~Peer() = default;

  // Run peer_thread_main from Raft_consensus
  void Run(std::shared_ptr<Peer> self) noexcept;
  void Shutdown() noexcept {
    exiting_ = true;
  }

  // void Interrupt() noexcept;

  void BeginRequestVote() noexcept;
  void UpdatePeer() noexcept;
  bool HaveVote() const noexcept {
    return have_vote_;
  }

  Index const &GetMatchIndex() const noexcept {
    return match_index_;
  }

 private:
  uint64_t id_;
  RaftRef raft_;
  std::unique_ptr<RaftService::Stub> stub_;

  bool exiting_ = false;
  bool vote_request_done_ = false;
  bool have_vote_ = false;

  TimePoint next_heartbeat_time_ = TimePoint::min();
  TimePoint prorogue_for_ = TimePoint::min();
  Index next_index_;
  Index match_index_ = 0;

  Counter failed_rpc_num_ = 0;

  friend class RaftConsensus;
  friend class RaftPool;
};

}  // namespace raft
}  // namespace abeille

#endif  // ABEILLE_RAFT_PEER_H_
