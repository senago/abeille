#include "raft/consensus/log/include/log.hpp"

#include <fstream>

#include "common/utils/include/logger.hpp"

namespace abeille {
namespace raft {

// ---------------------- Getters ----------------------------//

Index Log::LastIndex() const noexcept {
  // std::lock_guard<std::mutex> lck(mutex_);
  if (log_.empty()) {
    return StartIndex() - 1;
  }

  return log_.size() + start_index_ - 1;
}

Index Log::StartIndex() const noexcept {
  // std::lock_guard<std::mutex> lck(mutex_);
  return start_index_;
}

Log::EntryConstReference Log::operator[](size_t pos) const {
  // std::lock_guard<std::mutex> lck(mutex_);
  return log_[pos - start_index_];
}

bool Log::HasEntryWith(Index idx, Term term) noexcept {
  std::lock_guard<std::mutex> lck(mutex_);
  if (idx < start_index_ || idx > LastIndex()) {
    return false;
  }

  return (term ? (log_[--idx].term() == term) : true);
}

size_t Log::Size() const noexcept {
  return log_.size();
}

// ---------------------- Modifiers ----------------------------//

// These one cannot be commited and, consequently snapshoted
size_t Log::DropAfter(Index index) noexcept {
  std::lock_guard<std::mutex> lck(mutex_);
  size_t count = 0;
  while (LastIndex() != index) {
    if (!log_.empty()) {
      log_.pop_back();
      ++count;
    } else {
      break;
    }
  }

  return count;
}

void Log::Append(Log::EntryConstReference entry) noexcept {
  std::lock_guard<std::mutex> lck(mutex_);
  log_.push_back(entry);
}

size_t Log::Discard() noexcept {
  std::lock_guard<std::mutex> lck(mutex_);
  size_t count = log_.size();

  if (count) {
    log_.clear();
    start_index_ = count;
  }

  return count;
}

size_t Log::DropBefore(Index idx) noexcept {
  std::lock_guard<std::mutex> lck(mutex_);

  if (log_.empty()) {
    start_index_ = idx;
    return 0;
  }

  size_t count = 0;
  for (Index i = start_index_; i < idx; ++i, ++count) {
    if (!log_.empty()) {
      log_.pop_front();
    } else {
      break;
    }
  }

  if (count) {
    start_index_ = idx;
  }

  return count;
}

void Log::SetStartIndex(Index idx) noexcept {
  std::lock_guard<std::mutex> lck(mutex_);
  start_index_ = idx;
}

}  // namespace raft
}  // namespace abeille
