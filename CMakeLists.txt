cmake_minimum_required(VERSION 3.18)

project(abeille)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_EXPORT_COMPILE_COMMANDS ON)
set(CMAKE_POSITION_INDEPENDENT_CODE ON)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin)

option(ENABLE_DEBUG "Enable debug mode" OFF)
option(ENABLE_TESTS "Enable tests" OFF)
option(ENABLE_CPPCHECK "Enable cppcheck" OFF)
option(ENABLE_COVERAGE "Enable coverage" OFF)
option(ENABLE_SANITIZERS "Enable sanitizers" OFF)

# Temporary
add_compile_options(-g)

add_compile_options(-Wpedantic -Wall -Wextra -Wshadow)
add_compile_options(-Wno-unused-parameter -Wno-unused-function -Wno-sign-compare)

if(ENABLE_DEBUG)
  add_compile_definitions(DEBUG)
endif()

if(ENABLE_TESTS)
  enable_testing()
  find_package(GTest REQUIRED)
  include_directories(${GTEST_INCLUDE_DIRS})
  list(APPEND CMAKE_CTEST_ARGUMENTS "--output-on-failure")
endif()

if(ENABLE_CPPCHECK)
find_program(CMAKE_CXX_CPPCHECK cppcheck)
  if(CMAKE_CXX_CPPCHECK)
    list(
      APPEND CMAKE_CXX_CPPCHECK
      "--enable=warning,style,performance,portability" "--quiet" "-j 4"
      "-i proto" "--suppress=preprocessorErrorDirective"
      "common" "raft" "user" "worker"
    )
      
    add_custom_target(cppcheck
      COMMAND ${CMAKE_CXX_CPPCHECK}
      WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
      COMMENT "Static code analysis using cppcheck"
    )
  endif()
endif()

if(ENABLE_COVERAGE)
  add_compile_options(--coverage -g -O0)
  add_link_options(--coverage)
endif()

if(ENABLE_SANITIZERS)
  add_compile_options(-lasan)
  add_compile_options(-fsanitize=leak)
  add_compile_options(-fsanitize=undefined)
  add_compile_options(-fno-omit-frame-pointer -fsanitize=address)
  add_link_options(-lasan)
  add_link_options(-fsanitize=leak)
  add_link_options(-fsanitize=undefined)
  add_link_options(-fno-omit-frame-pointer -fsanitize=address)
elseif(NOT ENABLE_COVERAGE)
  add_compile_options("-O2")
endif()

add_subdirectory(common)

add_subdirectory(user)
add_subdirectory(raft)
add_subdirectory(worker)
add_subdirectory(cpp-linenoise)

