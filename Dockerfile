FROM alpine:edge

WORKDIR /app
COPY . ./
RUN apk update && \
  apk add --no-cache build-base grpc-dev protoc protobuf-dev re2-dev c-ares-dev cmake bash vim cppcheck py3-pip && \
  pip3 install cpplint --upgrade && \
  rm -rf /tmp/* /var/tmp/*
