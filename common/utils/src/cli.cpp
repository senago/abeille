#include "common/utils/include/cli.hpp"

#include <algorithm>

namespace abeille {
namespace common {

void CLI::SetHelpers(handlers_helpers &&helpers) noexcept {
  helpers_ = helpers;
  helpers_.push_back({HELP_COMMAND, "lists available functions"});
  helpers_.push_back({EXIT_COMMAND, "terminates the programm"});
  for (const auto &helper : helpers_) {
    if (helper.command.size() > max_command_size_) {
      max_command_size_ = helper.command.size();
    }
  }
}

bool CLI::Process(std::string &line) {
  args_type args = Split(line, ' ');
  if (args.empty()) {
    std::cout << "cli: empty args" << std::endl;
    return exit_;
  }

  std::string command = args[0];
  if (command == HELP_COMMAND) {
    Help();
  } else if (command == EXIT_COMMAND) {
    Exit();
  } else {
    std::cout << CommandHandler(args) << std::endl;
  }

  return exit_;
}

void CLI::Help() const noexcept {
  for (const auto &value : helpers_) {
    std::cout << "-- " << std::left << std::setw((3 * max_command_size_) / 2);
    std::cout << value.command << value.description << std::endl;
  }
}

void CLI::Exit() noexcept {
  exit_ = true;
}

}  // namespace common
}  // namespace abeille
