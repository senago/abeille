#include "common/rpc/include/server.hpp"

#include "common/utils/include/logger.hpp"

namespace abeille {
namespace rpc {

Server::Server(Server &&other) noexcept
    : services_(std::move(other.services_)),
      thread_(std::move(other.thread_)),
      server_(std::move(other.server_)) {}

Server &Server::operator=(Server &&other) noexcept {
  if (this != &other) {
    services_ = std::move(other.services_);
    thread_ = std::move(other.thread_);
    server_ = std::move(other.server_);
  }
  return *this;
}

error Server::Run() {
  init();
  LOG_INFO("launching the server...");
  thread_ = std::thread(std::thread(&Server::launch_and_wait, this));

  std::unique_lock<std::mutex> lk(mutex_);
  cv_.wait(lk, [&] { return ready_; });

  if (server_ == nullptr) {
    return error("failed to build and start the server");
  }

  LOG_INFO("server is running");
  return error();
}

void Server::Shutdown() {
  if (!shutdown_) {
    shutdown_ = true;
    LOG_INFO("Shutting down the server...");

    if (server_) {
      server_->Shutdown(std::chrono::system_clock::now());
      server_->Wait();
    }

    if (thread_.joinable()) {
      thread_.join();
    }

    LOG_INFO("Server shutdown successfully...");
  }
}

void Server::init() {
  LOG_INFO("registering services...");
  for (const auto &service : services_) {
    try {
    builder_.AddListeningPort(service.address,
                              grpc::InsecureServerCredentials());
    } catch (...) {
      LOG_ERROR("Wrong format of address %s", service.address.c_str());
    }

    builder_.RegisterService(service.address, service.service);
  }
}

void Server::launch_and_wait() {
  {
    std::lock_guard<std::mutex> lk(mutex_);
    server_ = builder_.BuildAndStart();
    ready_ = true;
  }
  cv_.notify_one();
  if (server_ != nullptr) {
    server_->Wait();
  }
}

}  // namespace rpc
}  // namespace abeille
