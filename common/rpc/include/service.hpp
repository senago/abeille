#ifndef ABEILLE_RPC_SERVICE_HPP_
#define ABEILLE_RPC_SERVICE_HPP_

#include <grpc/grpc.h>

#include <thread>

#include "common/utils/include/convert.hpp"

using grpc::ServerContext;
using grpc::ServerReaderWriter;
using grpc::Status;

namespace abeille {
namespace rpc {

static constexpr const auto SERVICE_COOLDOWN = std::chrono::milliseconds(500);

template <typename ConnReq, typename ConnResp, typename Svc>
class Service : public Svc {
 public:
  using ConnectStream = grpc::ServerReaderWriter<ConnResp, ConnReq>;

  virtual void ConnectHandler(uint64_t client_id) {}
  virtual void CommandHandler(uint64_t client_id, ConnResp &resp) = 0;
  virtual void StatusHandler(uint64_t client_id, const ConnReq &req) = 0;
  virtual void DisconnectHandler(uint64_t client_id) {}

 private:
  Status Connect(ServerContext *context, ConnectStream *stream) override {
    std::string address = ExtractAddress(context->peer());
    uint64_t client_id = address2uint(address);
    ConnectHandler(client_id);

    ConnReq req;
    while (stream->Read(&req)) {
      StatusHandler(client_id, req);

      ConnResp resp;
      CommandHandler(client_id, resp);
      bool ok = stream->Write(resp);
      if (!ok) {
        break;
      }

      std::this_thread::sleep_for(SERVICE_COOLDOWN);
    }

    DisconnectHandler(client_id);

    return Status::OK;
  }
};

}  // namespace rpc
}  // namespace abeille

#endif  // ABEILLE_RPC_SERVICE_HPP_
